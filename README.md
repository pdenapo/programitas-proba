# Programitas para mis alumnos de probabilidad y estadística. 

Estos programas corresponden a los ejemplos y gráficos que mostramos en 
las clases, Están realizados en [Python 3](https://www.python.org/). 

Contiene archivos de las clases de

* Probabilidades y Estadística para matemática. Segundo cuatrimestre de 2021.

* Probabilidades y Estadística para computación. Primer cuatrimestre de
  2023.

(C) 2021-2023 Pablo De Nápoli.

La filosofìa es que me inspira a compartirles estos programas es que 
matemática y computación van de la mano. En el siglo
XXI, no se puede hacer matemática sin computadoras. Esto es especialmente 
cierto en la matemática aplicada y la ciencia de datos. Y por otra parte, 
programar ayuda a aprender a pensar matemáticamente.

Los archivos .ipynb son  notebooks para utlizar usando [Jupyter](https://jupyter.org/). Este programa proporciona una interfaz gráfica amigable en un navegador web, muy útil para probar cosas (en Python y otros lenguajes) y mostrar los resultados en forma de gráficos o tablas de forma interactiva. Es muy usada 
en aplicaciones científicas o de ciencia de datos.

La siguiente es una lista de materiales útiles para aprender a programar
y para aprender a usar las herramientas que utilicé durante el curso (pero 
aclaro que NO es necesario que las manejen para aprobarlo, ya que como van a ver es algo bastante complejo).

##  Materiales recomendados para aprender Python

* [Matemáticas y programación con Python](https://www.oma.org.ar/invydoc/docs-libro/apuntes.pdf). Excelente libro por el colega santafesino Nestor Aguilera, en la página de la OMA. Si no saben programar en Python, les recomiendo empezar por acá, ya que se enfoca en mi misma filosofía de que programar enseña a pensar matemáticamente.

* [Essential Statistics with Python and R](https://www.researchgate.net/publication/334896049_Essential_Statistics_with_Python_and_R) Excelente libro por [Sreenivasa Rao Jammalamadaka](https://ml.ucsb.edu/people/faculty/sreenivasa-rao-jammalamadaka) (en la página de su autor). Es un libro 
de estadística con un enfoque práctico, que muestra como programar las diferentes cosas en Python o en 
[R](https://www.r-project.org/), otro software muy usado para estadística. Personalmente prefiero usar Python porque es un lenguaje de propósito general muy popular, que se utiliza para muchas otras aplicaciones, y posiblemente ya lo hayan usado en otras materias.

* [How to Think Like a Computer Scientist: Learning with Python 3 ](https://openbookproject.net/thinkcs/python/english3e/). Excelente libro que explica los conceptos fundamentales de la programación con Python. Es interesante porque 
focaliza en estos conceptos, y no tanto en el lenguaje.

Hay una [versión en castellano](https://argentinaenpython.com/quiero-aprender-python/aprenda-a-pensar-como-un-programador-con-python.pdf) pero usa la versión 2 de Python en lugar de la 3. Hay algunas importantes diferencias entre estas dos versiones del lenguaje, para programas nuevos es recomendable usar siempre la 3. En general, los materiales en inglés suelen estar más actualizados.

* [Tutorial de Python del canal Socratica](https://www.youtube.com/watch?v=bY6m6_IIN94&list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-) 
Es excelente, y muy divertido. Está en inglés (pero yo recomiendo escuchar videos de programación en este idioma para fijar la pronunciación  correcta de las palabras).

*[Python notes for professionals](https://books.goalkicker.com/PythonBook/)

## Biblioteca Utilizadas

Los programas de este repositorio utilizan una serie de bibliotecas (o librerías), de uso muy común en aplicaciones científicas, y en particular en la ciencia de datos.

*La biblioteca [Scipy](https://www.scipy.org/) nos proporciona todas las 
 distribuciones más comunes de probabilidad ya programadas.
 
*A su vez, esta biblioteca usa [Numpy](https://numpy.org/) para trabajar en 
 forma eficiente con vectores en Python.

*También podemos utilizar [Matplolib](https://matplotlib.org/), [Seaborn](https://seaborn.pydata.org/) o 
[Plotly](https://plotly.com/python/) para realizar los gráficos.

*Y [Pandas](https://pandas.pydata.org/) para analizar conjuntos de datos.

## Referencias para aprender a usarlas

*[Scipy Lecture Notes](https://scipy-lectures.org/) Introducción a las librerías numpy, matplotlib y scipy que estoy usando en
estos programitas. Hay una [versión en castellano](https://claudiovz.github.io/scipy-lecture-notes-ES/index.html) pero usa Python 2.

*[Guide to Numpy](http://www.astro-udec.cl/juin/PythonCourse/Help/Numpy_Book.pdf) Guía que explica en profundidad como funciona Numpy.

*[Excelente tutorial de Scipy en Youtube](https://www.youtube.com/watch?v=jmX4FOUEfgU)

*[Serie de tutoriales sobre Pandas en Yotube](https://www.youtube.com/playlist?list=PL-osiE80TeTsWmV9i9c58mdDCSskIFdDS)


## Otras herramientas computacionales que es bueno que conzcan

* Como hemos dicho más arriba, otro software muy popular para estadística y ciencia de datos es [R](https://www.r-project.org/). Me preguntaron durante 
el curso si podía recomendarles algún libro sobre él y se me ocurrió
[R Notes for professionals](https://books.goalkicker.com/RBook/).
También hay [Tutoriales en youtube](https://www.youtube.com/watch?v=_V8eKsto3Ug).

* Un software muy recomendado para matemática es [Sagemath](https://www.sagemath.org/) que provee una interfaz común en Python para muchas librerías matemáticas.
Permite usar todas las herramientas que mencionamos antes (incluido R y la 
interfaz de Jupyter).

* Para escribir todo lo que se escribe en matemática (incluidos: las prácticas,
parciales y material de la clase que usamos en la materia) utilizamos LaTeX Probablemente la forma más sencilla de utilizarlo (sin instalar nada en sus máquinas), es usar [overleaf](https://www.overleaf.com/). 

Es muy recomendable que aprendan LaTeX porque les será de gran ayuda por ejemplo
cuando tengan que escribir su tesis de licenciatura. Mi recomendación para
aprender a usarlo es [A (Not So) Short Introduction to LaTeX2ε](https://www.ctan.org/tex-archive/info/lshort). Hay [versión en castellano](https://www.ctan.org/tex-archive/info/lshort/spanish).

También pueden consultar [LaTeX Notes for Professionals](https://books.goalkicker.com/LaTeXBook/).


