import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import scipy.stats as stats

# create the distribution (exponencial con parámetro 1)
distribution = stats.expon()
# create x's
xs = np.linspace(-0.5, 3, 100001)

# create random samples
n = 1000000
#samples = distribution.rvs(size=n)

U = stats.uniform.rvs(size=n)
X = -np.log(1 - U)

hist = sns.histplot(data=X, stat='density', color='Orange', bins=50)
pdfs = distribution.pdf(xs)
plt.plot(xs, pdfs, color='red')

# label them
plt.title('Densidad exponencial vs simulación', fontsize=24)
plt.xlabel('x', fontsize=20)
plt.ylabel('Densidad', fontsize=20)
plt.savefig('simular_exponencial.png')
plt.show()