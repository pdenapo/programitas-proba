import scipy.stats
import numpy as np

xk = (1, 2, 3)
pk = (0.5, 0.25, 0.25)
n = len(xk)

uniforme = scipy.stats.uniform

a = {}
a[0] = 0
for k in range(1, n):
    print("k=", k)
    print("pk[k]=", pk[k])
    a[k] = a[k - 1] + pk[k - 1]
    print("a=", a)

a[n] = 1


def X():
    U = uniforme.rvs()
    for k in range(0, n):
        if a[k] < U and U <= a[k + 1]:
            return k, xk[k]


frecuencia = np.zeros(7, dtype=int)
cuantas_veces = 10000
for i in range(0, cuantas_veces):
    indice, resultado = X()
    print(resultado)
    frecuencia[indice] += 1

print("xk=", xk)
print("pk=", pk)
print("a=", a)

print("cuantas_veces=", cuantas_veces)
print("valor \tfrecuencia \t f. relativa \t probabilidad ")
for k in range(0, n):
    print(xk[k], "\t", frecuencia[k], "\t\t", frecuencia[k] / cuantas_veces,
          pk[k])
