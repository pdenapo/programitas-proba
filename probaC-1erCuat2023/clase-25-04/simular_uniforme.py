import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import scipy

cuantas_veces = 10000
veces = np.zeros(6, dtype=int)

U = scipy.stats.uniform
X = U.rvs(size=cuantas_veces)

hist = sns.histplot(data=X, stat='density', color='Orange', bins=50)
plt.title('Densidad Uniforme vs simulación ' + str(cuantas_veces) + " veces",
          fontsize=18)
plt.xlabel('x', fontsize=18)
plt.ylabel('Densidad', fontsize=18)
plt.savefig('spectrum.png')
plt.show()