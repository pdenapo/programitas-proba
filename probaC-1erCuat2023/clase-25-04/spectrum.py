# simula el generador de números pseudo-aleatorios de Lehmer
# usado por el ZX-spectrum
# https://en.wikipedia.org/wiki/Lehmer_random_number_generator

p = 2**16 + 1
g = 75
x = 1  #semilla del generador


def generador():
    global x
    x = (x * g) % p
    return x / p


import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

cuantas_veces = 100000
veces = np.zeros(6, dtype=int)

X = []
for k in range(0, cuantas_veces):
    X.append(generador())

hist = sns.histplot(data=X, stat='density', color='Orange', bins=50)
plt.title('Densidad Uniforme vs simulación ' + str(cuantas_veces) + " veces",
          fontsize=18)
plt.xlabel('x', fontsize=18)
plt.ylabel('Densidad', fontsize=18)
plt.savefig('spectrum.png')
plt.show()