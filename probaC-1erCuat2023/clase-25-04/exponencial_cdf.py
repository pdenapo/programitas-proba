# usar python3 
import numpy as np
import scipy.stats 
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
hasta=10
x=np.arange(start=-1,stop=hasta,step=0.1)
lam=1
exponencial=scipy.stats.expon(scale=1/lam)
y=exponencial.cdf(x)
maximo_y=max(y)
plt.axis([-1, hasta, -0.01, maximo_y*1.1])
plt.xlabel("x")
plt.ylabel("probabilidad")
plt.plot(x,y,color='blue')
plt.text(3,maximo_y*0.9,'Distribución exponencial',color='blue')
plt.text(3,maximo_y*0.85,'con $\lambda$='+str(lam),color='blue')
plt.savefig("../graficos/exponencial_cdf.png")
plt.grid()
plt.show() 