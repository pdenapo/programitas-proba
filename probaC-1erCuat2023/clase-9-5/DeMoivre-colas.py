# usar python3 
import numpy as np
import scipy.stats 
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
n=3000
p=0.001
binomial=scipy.stats.binom(n,p)
x=np.arange(start=0,stop=n)
x_denso=np.arange(start=0,stop=n,step=0.1)
y=binomial.pmf(x)
maximo_y= max(y)
esperanza=n*p
varianza=n*p*(1-p)
normal=scipy.stats.norm(loc=esperanza,scale=np.sqrt(varianza))
z=normal.pdf(x_denso)
#poisson=scipy.stats.poisson(mu=esperanza)
#w=poisson.pmf(x)
plt.axis([0, 10, 0, maximo_y*1.1])
plt.xlabel("k")
plt.ylabel("probabilidad")
#plt.title("La distribución binomial con n="+str(n)+" y p="+str(p))
plt.plot(x,y,'ro')
plt.text(6,maximo_y*0.9,'Distribución binomial',color='red')
plt.text(6,maximo_y*0.85,'con n='+str(n)+",p="+str(p),color='red')
plt.plot(x_denso,z,color='blue')
plt.text(6,maximo_y*0.8,'Aproximación por normal',color='blue')
#plt.plot(x,w,'mo')
#plt.text(0.5,maximo_y*0.75,'Aproximación de Poisson',color='m')
plt.show() 