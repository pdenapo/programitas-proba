# usar python3 
import numpy as np
import scipy.stats 
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
mu=6
distribucion=scipy.stats.poisson(mu)
x=np.arange(start=0,stop=20)
y=distribucion.pmf(x)
y_maximo=max(y)*1.1
plt.axis([0, 20, 0, y_maximo])
plt.xlabel("k")
plt.ylabel("probabilidad")
plt.text(10,0.15,'La distribución de Poisson con $\lambda$='+str(mu),color='r')
#plt.title("La distribución binomial con n="+str(n)+" y p="+str(p))
grafico=plt.plot(x,y,'ro')
plt.show() 