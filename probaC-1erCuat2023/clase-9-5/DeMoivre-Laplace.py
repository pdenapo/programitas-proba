# usar python3 
import numpy as np
import scipy.stats 
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

def binomial_cdf_reescalada (x,n,p):
 esperanza=n*p
 varianza=n*p*(1-p)
 escala= np.sqrt(varianza)
 x_reescalado= escala*x + esperanza
 return scipy.stats.binom.cdf(x_reescalado,n,p)

desde= -5
hasta= 5
p=0.4 
x=np.arange(start=desde,stop=hasta,step=0.01)

binomial5= binomial_cdf_reescalada(x,5,p)
binomial25= binomial_cdf_reescalada(x,25,p)
binomial50= binomial_cdf_reescalada(x,50,p)
binomial100= binomial_cdf_reescalada(x,100,p)
normal=scipy.stats.norm.cdf(x)
plt.axis([desde,hasta, -0.1, 1.1])
plt.ylabel("probabilidad")
x_texto=-3.9
plt.text(x_texto,1,'p='+str(p),color='black')
plt.plot(x,binomial5,color='green')
plt.text(x_texto,0.9,'binomial reescalada n=5',color='green')
plt.plot(x,binomial25,color='blue')
plt.text(x_texto,0.8,'binomial reescalada n=25',color='blue')
plt.plot(x,binomial50,color='black')
plt.text(x_texto,0.7,'binomial reescalada n=50',color='black')
plt.plot(x,binomial100,color='magenta')
plt.text(x_texto,0.6,'binomial reescalada$ n=100',color='magenta')

plt.plot(x,normal,color='red')
plt.text(x_texto,0.5,'normal estándar',color='red')
plt.show() 