import matplotlib.pyplot as plt
import numpy as np
import seaborn
import scipy.stats as stats

#rng = np.random.default_rng()

# simulamos un dado
xk = np.arange(start=1, stop=7)
p = 1 / 6
pk = (p, p, p, p, p, p)
distribucion = stats.rv_discrete(values=(xk, pk))

#distribucion = stats.bernoulli(0.5)


# Realizamos k veces el experimento compuesto de calcular Sn_star
def ensayar(distribucion, n, k):
    mu = distribucion.mean()
    sigma = distribucion.std()
    print("mu=", mu)
    print("sigma=", sigma)
    ensayos = np.array([])
    for i in range(k):
        Sn = np.sum(distribucion.rvs(size=n))
        Sn_star = (Sn - n * mu) / (np.sqrt(n) * sigma)
        ensayos = np.append(ensayos, Sn_star)
    return ensayos


n = 10000
k = 10000
ensayos = ensayar(distribucion, n, k)

#print("n_mu=", np.mean(ensayos))
#print("n_sigma=", np.std(ensayos))

seaborn.histplot(data=ensayos, stat='density', color='Orange')
# puedo elegir cuantos bins con bins=10

# create x's
xs = np.linspace(-4, 4, 100001)
pdfs = stats.norm.pdf(xs)
plt.plot(xs, pdfs, color='red')

# label them
plt.title('Densidad normal vs histograma', fontsize=24)
plt.xlabel('x', fontsize=20)
plt.ylabel('Densidad', fontsize=20)
plt.savefig("../graficos/tcl_dado.png")
#plt.savefig("../graficos/tcl_moneda.png")
plt.show()
