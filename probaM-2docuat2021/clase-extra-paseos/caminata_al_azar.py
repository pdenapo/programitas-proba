import sys
#from types import DynamicClassAttribute
import pygame
from pygame.locals import KEYDOWN, K_q
import random
import argparse
from math import pi

# CONSTANTS:
SCREENSIZE = WIDTH, HEIGHT = 800, 800
PADDING_LEFT = 50
PADDING_RIGHT = 50
PADDING_TOP = 50
PADDING_BOTTOM = 50
ANCHO_UTIL = WIDTH - PADDING_LEFT - PADDING_RIGHT
ALTURA_UTIL = HEIGHT - PADDING_TOP - PADDING_BOTTOM
ORIGEN_X = PADDING_LEFT + ANCHO_UTIL // 2
ORIGEN_Y = PADDING_TOP + ALTURA_UTIL // 2
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
MAGENTA = (255, 0, 255)

R = 15
h = 20
x_inicial = 0
y_inicial = 0


def matematicas_a_fisicas(x, y):
    return (ORIGEN_X + x, ORIGEN_Y - y)


def fisicas_a_matematicas(x, y):
    return (x - ORIGEN_X, ORIGEN_Y - y)


def main(mostrar_trayectoria, FPS, con_borde):
    redibujarlo = False
    pygame.init()
    pygame.display.set_caption('Caminata al azar')
    clock = pygame.time.Clock()
    global surface
    pantalla = pygame.display.set_mode(SCREENSIZE)
    primera_vez = True

    while True:
        clock.tick(FPS)
        if primera_vez:
            redibujarlo = False
            evento = ""
        else:
            evento = checkEvents()
            if evento == "reset":
                redibujarlo = True
            elif evento == "save":
                pygame.image.save(pantalla, "paseo.png")

        if primera_vez or redibujarlo or not (mostrar_trayectoria):
            #dibujamos la grilla y el círculo rojo
            pantalla.fill(WHITE)
            drawgrid(pantalla)

            # dibujamos la parte superior del círculo en rojo y la inferior en magenta

            if con_borde:
                pygame.draw.arc(
                    pantalla, RED,
                    pygame.Rect(matematicas_a_fisicas(-R * h, R * h),
                                (2 * R * h, 2 * R * h)), 0, pi, 2)

                pygame.draw.arc(
                    pantalla, MAGENTA,
                    pygame.Rect(matematicas_a_fisicas(-R * h, R * h),
                                (2 * R * h, 2 * R * h)), pi, 2 * pi, 2)

            #pygame.draw.circle(pantalla, RED, matematicas_a_fisicas(0, 0),
            #                   R * h, 2)

        if primera_vez or redibujarlo:
            pos_x = x_inicial
            pos_y = y_inicial
            vivo = True
            redibujarlo = False

        anterior_x = pos_x
        anterior_y = pos_y
        if primera_vez:
            primera_vez = False
        else:
            # actualizamos las coordenadas
            azar = random.randint(0, 3)
            if azar == 0:
                pos_x += h
            elif azar == 1:
                pos_x -= h
            elif azar == 2:
                pos_y += h
            elif azar == 3:
                pos_y -= h

        if vivo:
            # dibujamos el cursor
            if mostrar_trayectoria:
                pygame.draw.line(pantalla, BLUE,
                                 matematicas_a_fisicas(anterior_x, anterior_y),
                                 matematicas_a_fisicas(pos_x, pos_y), 4)
            else:
                pygame.draw.circle(pantalla, BLUE,
                                   matematicas_a_fisicas(pos_x, pos_y), 8, 0)

        # vemos si estamos adentro del círculo
        if con_borde:
            adentro = pos_x**2 + pos_y**2 < (R * h)**2
            if not (adentro):
                vivo = False

        pygame.display.flip()


def drawgrid(surface):
    color_elegido = BLACK
    for i in range(-R, R + 1):
        y = i * h
        if i == 0:
            grosor = 4
        else:
            grosor = 1
        pygame.draw.line(surface, color_elegido,
                         matematicas_a_fisicas(-R * h, y),
                         matematicas_a_fisicas(R * h, y), grosor)
    for i in range(-R, R + 1):
        x = i * h
        if i == 0:
            grosor = 4
        else:
            grosor = 1
        pygame.draw.line(surface, color_elegido,
                         matematicas_a_fisicas(x, -R * h),
                         matematicas_a_fisicas(x, R * h), grosor)


def checkEvents():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == KEYDOWN and event.key == K_q:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
            return "reset"
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_s:
            return "save"
        else:
            return ""


if __name__ == "__main__":
    global mostrar_trayectoria

    parser = argparse.ArgumentParser(description='Caminata al azar')
    parser.add_argument("-t", action='store_true', help='mostrar trayectoria')
    parser.add_argument("-b", action='store_true', help='con borde')
    parser.add_argument("--fps",
                        action='store',
                        help='cuadros por segundo',
                        default='30',
                        type=int)
    args = parser.parse_args()
    main(args.t, args.fps, args.b)
