import sys
#from types import DynamicClassAttribute
import pygame
from pygame.locals import KEYDOWN, K_q
import random
import argparse
from math import pi

# CONSTANTS:
SCREENSIZE = WIDTH, HEIGHT = 800, 800
PADDING_LEFT = 50
PADDING_RIGHT = 50
PADDING_TOP = 50
PADDING_BOTTOM = 50
ANCHO_UTIL = WIDTH - PADDING_LEFT - PADDING_RIGHT
ALTURA_UTIL = HEIGHT - PADDING_TOP - PADDING_BOTTOM
ORIGEN_X = PADDING_LEFT + ANCHO_UTIL // 2
ORIGEN_Y = PADDING_TOP + ALTURA_UTIL // 2
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
MAGENTA = (255, 0, 255)

R = 15
h = 20
x_inicial = 0
y_inicial = 0


def matematicas_a_fisicas(x, y):
    return (ORIGEN_X + x, ORIGEN_Y - y)


def fisicas_a_matematicas(x, y):
    return (x - ORIGEN_X, ORIGEN_Y - y)


def drawgrid(surface):
    color_elegido = BLACK
    for i in range(-R, R + 1):
        y = i * h
        if i == 0:
            grosor = 4
        else:
            grosor = 1
        pygame.draw.line(surface, color_elegido,
                         matematicas_a_fisicas(-R * h, y),
                         matematicas_a_fisicas(R * h, y), grosor)
    for i in range(-R, R + 1):
        x = i * h
        if i == 0:
            grosor = 4
        else:
            grosor = 1
        pygame.draw.line(surface, color_elegido,
                         matematicas_a_fisicas(x, -R * h),
                         matematicas_a_fisicas(x, R * h), grosor)


def main():
    pygame.init()
    pygame.display.set_caption('Caminata al azar')
    pantalla = pygame.display.set_mode(SCREENSIZE)
    pantalla.fill(WHITE)
    drawgrid(pantalla)
    pygame.display.flip()
    pygame.image.save(pantalla, "grilla.png")

    while True:
        pass


if __name__ == "__main__":
    main()
