# Simple pygame program

# Import and initialize the pygame library

import pygame
import random

import math

pygame.init()

pygame.display.set_caption('Eligiendo un número real al azar')

BLUE = (0, 0, 255)
BLACK = (0, 0, 0)

# Set up the drawing window

largo = 1000
ancho = 800
screen = pygame.display.set_mode([largo, ancho])

# Set the font

font = pygame.font.SysFont(None, 24)


def experimento():
    # Fill the background with white
    screen.fill((255, 255, 255))

    a = 0.0
    b = 1.0
    y = 0

    cuantas_etapas = 8
    numero_armado = ""

    for etapa in range(0, cuantas_etapas):
        if etapa > 0:
            moneda = random.randint(0, 1)
            if moneda == 1:
                a = (a + b) / 2
                texto = ": sale cara"
                numero_armado += "1"
            else:
                b = (a + b) / 2
                texto = ": sale ceca"
                numero_armado += "0"
            texto += " x= " + "0." + numero_armado + " bin"
        else:
            texto = ""

        y = y + largo / (2 * cuantas_etapas + 5)

        img = font.render('Etapa ' + str(etapa) + texto, True, BLACK)
        screen.blit(img, (ancho * 0.1, y))

        y = y + largo / (2 * cuantas_etapas + 5)

        # reescalamos el intervalo

        x_inicial = ancho * 0.1 + a * ancho * 0.9
        x_final = ancho * 0.1 + b * ancho * 0.9

        pygame.draw.line(screen, BLUE, [x_inicial, y], [x_final, y], 5)

        pygame.image.save(screen, "numero_al_azar.png")
        # Flip the display
        pygame.display.flip()


# Run until the user asks to quit
running = True
dibujarlo = True

while running:
    if dibujarlo:
        experimento()
        dibujarlo = False
    for event in pygame.event.get():
        # Did the user click the window close button?
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                dibujarlo = True

# Done! Time to quit.

pygame.quit()