# usar python3 
import numpy as np
import scipy.stats 
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
n=20
p=0.5
distribucion=scipy.stats.binom(n,p)
x=np.arange(20)
y=distribucion.pmf(x)
np.set_printoptions(precision=10,suppress=True)
print(y)
plt.axis([0, 20, 0, 0.20])
plt.xlabel("k")
plt.ylabel("probabilidad")
#plt.title("La distribución binomial con n="+str(n)+" y p="+str(p))
grafico=plt.plot(x,y,'ro')
plt.show()
 