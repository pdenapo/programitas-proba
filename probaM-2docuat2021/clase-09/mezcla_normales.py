import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

x = np.arange(-7, 7, 0.001)
y = norm.pdf(x, loc=-3) / 2 + norm.pdf(x, loc=3) / 2

plt.plot(x, y)
plt.grid()
plt.savefig('../graficos/mezcla_normales.png', dpi=72, bbox_inches='tight')
plt.show()