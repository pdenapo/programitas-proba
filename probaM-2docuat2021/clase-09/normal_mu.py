# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt

plt.xlabel("x")
plt.ylabel("probabilidad")
plt.grid()
x = np.arange(start=-8, stop=8, step=0.01)

sigma = 1
for mu in [0, -4, -2, 2, 4]:
    distribucion = scipy.stats.norm(mu, sigma)
    y = distribucion.pdf(x)
    plt.plot(x, y, label=r'$\mu$=' + str(mu))

plt.legend()
plt.savefig('../graficos/normal_mu.png')

plt.show()
