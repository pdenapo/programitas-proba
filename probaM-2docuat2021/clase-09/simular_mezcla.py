import numpy as np
#import matplotlib.pyplot as plt
from scipy.stats import norm

x = np.arange(-7, 7, 0.001)
#y = norm.pdf(x, loc=-3) / 2 + norm.pdf(x, loc=3) / 2
y1 = norm(loc=3)
y0 = norm(loc=-3)

rng = np.random.default_rng()

for vez in range(0, 10):
    moneda = rng.integers(0, 2)
    if moneda == 1:
        y = y1.rvs()
    else:
        y = y0.rvs()

    print("moneda=", moneda, "y=", y)
