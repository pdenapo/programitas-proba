# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
alpha = 5
lambda_ = 1
distribucion = scipy.stats.gamma(a=alpha, scale=1 / lambda_)
x = np.arange(start=0, stop=25, step=0.01)
y = distribucion.pdf(x)
y_maximo = max(y)
fig, ax = plt.subplots()
plt.axis([0, 25, 0, y_maximo * 1.1])
plt.xlabel("x")
plt.ylabel("probabilidad")
texto = r'La distribución gama con $\alpha=$' + str(
    alpha) + r',$,\lambda$=' + str(lambda_)
plt.text(7, 0.19, texto, fontsize='large')
media = distribucion.mean()
texto2 = "esperanza o media=" + np.format_float_positional(media, precision=5)
plt.text(7, 0.17, texto2, fontsize='large', color='g')
grafico = plt.plot(x, y, 'r')
mediana = distribucion.median()
texto3 = "esperanza o media =" + str(mediana)
plt.text(7, 0.15, texto3, fontsize='large', color='b')

# https://thispointer.com/find-max-value-its-index-in-numpy-array-numpy-amax/

maximo_donde = np.where(y == y_maximo)[0][0]
texto4 = "máximo de f = " + np.format_float_positional(y_maximo, precision=5)
plt.text(7, 0.13, texto4, fontsize='large', color='magenta')
moda = x[maximo_donde]
texto5 = "moda (donde se alcanza)= " + np.format_float_positional(moda,
                                                                  precision=5)
plt.text(7, 0.11, texto5, fontsize='large', color='magenta')

plt.grid()
fig.savefig('../graficos/gama.png')

plt.show()
