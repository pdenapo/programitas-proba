# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt

plt.xlabel("x")
plt.ylabel("probabilidad")
plt.grid()
x = np.arange(start=-8, stop=8, step=0.01)

mu = 0
for sigma in [0.5, 1, 2, 3]:
    distribucion = scipy.stats.norm(mu, sigma)
    y = distribucion.pdf(x)
    plt.plot(x, y, label=r'$\sigma$=' + str(sigma))

plt.legend()
plt.savefig('../graficos/normal_sigma.png')

plt.show()
