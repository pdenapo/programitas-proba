# usar python3 
import numpy as np
import scipy.stats 
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
hasta=10
x=np.arange(start=0,stop=hasta,step=0.1)
lam=1
exponencial=scipy.stats.expon(scale=1/lam)
y=exponencial.pdf(x)
maximo_y=max(y)
plt.axis([0, hasta, -0.01, maximo_y*1.1])
plt.xlabel("x")
plt.ylabel("probabilidad")
plt.plot(x,y,color='blue')
plt.text(2,maximo_y*0.9,'Densidad expoencial',color='blue')
plt.text(2,maximo_y*0.85,'con $\lambda$='+str(lam),color='blue')
plt.show() 