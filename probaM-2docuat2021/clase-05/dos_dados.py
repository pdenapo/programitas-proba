import numpy as np
x=np.zeros(13,dtype=int)
p=np.zeros(13,dtype=np.float)

np.set_printoptions(precision=3)
D=list(range(1,7))
for omega_1 in D:
  for omega_2 in D:
      s=omega_1+omega_2
      x[s] +=1
      
for k in range(1,13):
  p[k]=x[k]/36
  print(k,"&",x[k],"/36 &=",'%.3f' % p[k],"\\\\")
   