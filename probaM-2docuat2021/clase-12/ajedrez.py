import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.stats

# Datos sacados de Lichess el 15/9/2021
# canitdad de jugadores por cada elo
freq = [
    566, 337, 453, 572, 724, 813, 1048, 1223, 1467, 1706, 2110, 2341, 2787,
    2996, 3526, 3823, 4347, 4878, 5201, 5816, 6121, 6606, 7010, 7403, 7945,
    8110, 8254, 8451, 9007, 9250, 9354, 9729, 10026, 10313, 10143, 10155,
    10906, 10576, 10398, 10061, 10565, 10197, 10030, 9442, 9911, 9459, 9260,
    8639, 8935, 8261, 7809, 7163, 7450, 6704, 6173, 5422, 5968, 4956, 4549,
    3915, 3896, 3206, 2834, 2365, 2482, 1853, 1566, 1200, 1287, 892, 700, 555,
    562, 349, 280, 214, 197, 129, 93, 70, 51, 46, 24, 19, 14, 5, 5, 3, 4
]

freq = np.array(freq)
delta_elo = 25
elo_final = 600 + len(freq) * delta_elo
elos = np.arange(600, elo_final, delta_elo)

# imprimimos la tabla
#total_jugadores = 0
#elo = 600
#for f in freq:
#print(elo, f)
#total_jugadores += f
#elo += delta_elo
total_jugadores = np.sum(freq)
print("Número total de jugadores=", total_jugadores)
#print(len(elos))
freq_rel = freq / total_jugadores
data = scipy.stats.rv_discrete(values=(elos, freq_rel))
print("media=", data.mean())
print("varianza=", data.var())
print("desviación estándar=", data.std())
print("mediana=", data.median())
q1 = data.ppf(0.25)
q3 = data.ppf(0.75)
iqr = q3 - q1
print("iqr=", iqr)
max_f = np.amax(freq_rel)
print("máxima frecuencia relativa=", max_f)
donde = np.where(freq_rel == max_f)
print("moda=", elos[donde])

plt.plot(elos, freq_rel)
plt.grid()
plt.savefig('../../graficos/ajedrez_dibujado.png')
plt.show()
