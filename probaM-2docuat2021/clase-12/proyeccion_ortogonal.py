# https://mne.tools/0.21/auto_tutorials/preprocessing/plot_45_projectors_background.html

import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa
from scipy.linalg import svd
import mne


def setup_3d_axes():
    ax = plt.axes(projection='3d')
    ax.view_init(azim=-105, elev=20)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xlim(-1, 5)
    ax.set_ylim(-1, 5)
    ax.set_zlim(0, 5)
    return ax


ax = setup_3d_axes()

# plot the vector (3, 2, 5)
origin = np.zeros((3, 1))
point = np.array([[3, 2, 5]]).T
ax.text(point[0, 0] + 0.2, point[1, 0], point[2, 0], r'$x_0$', color='k')
vector = np.hstack([origin, point])
ax.plot(*vector, color='k')
ax.plot(*point, color='k', marker='o')

# project the vector onto the x,y plane and plot it
xy_projection_matrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 0]])
projected_point = xy_projection_matrix @ point
projected_vector = xy_projection_matrix @ vector
ax.plot(*projected_vector, color='C0')
ax.plot(*projected_point, color='C0', marker='o')
ax.text(projected_point[0, 0] + 0.3,
        projected_point[1, 0],
        projected_point[2, 0],
        r'$s_0$',
        color='C0')

ax.text(2, 0, 0, r'S', color='C0')

# add dashed arrow showing projection
arrow_coords = np.concatenate([point, projected_point - point]).flatten()
ax.quiver3D(*arrow_coords,
            length=0.96,
            arrow_length_ratio=0.1,
            color='C1',
            linewidth=1,
            linestyle='dashed')

plt.savefig("../graficos/proyeccion_ortogonal.png")
plt.show()