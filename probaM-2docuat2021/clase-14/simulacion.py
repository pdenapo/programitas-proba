#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Probando el algoritmo para simular la distribución normal a partir de la uniforme


# In[72]:


import numpy as np
N=1000
#N=100000
U= np.random.sample(N)
V= np.random.sample(N)
R= np.sqrt(-2*np.log(U))
W= 2*np.pi*V
X= R*np.cos(W)
Y= R*np.sin(W)


# In[73]:


G=np.hstack((X,Y))
G


# In[80]:


import seaborn as sb
import scipy
import matplotlib.pyplot as plt
ax=sb.histplot(G,stat='density')
x0, x1 = ax.get_xlim()  # extract the endpoints for the x-axis
x_pdf = np.linspace(x0, x1, 100)
y_pdf = scipy.stats.norm.pdf(x_pdf)

ax.plot(x_pdf, y_pdf, 'r', lw=2, label='pdf') 
plt.show()

