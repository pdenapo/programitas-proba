import numpy as np
cuantas_veces=10000
exitos=0
# np.random.seed(1) 
for n in range(1,cuantas_veces+1):
	dado= np.random.randint(1, 7)
	es_par = (dado==2) or  (dado==4) or (dado==6)
	if es_par:
		exitos +=1
	frecuencia= exitos/n 
	if n%1000==0:
		print (n,frecuencia)