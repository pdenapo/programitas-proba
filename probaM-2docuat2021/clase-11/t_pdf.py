# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
for n in range(1, 5):
    x = np.arange(start=-6, stop=6, step=0.01)
    distribucion = scipy.stats.t(n)
    y = distribucion.pdf(x)
    texto = r'n=' + str(n)
    plt.plot(x, y, label=texto)

distribucion = scipy.stats.norm
y = distribucion.pdf(x)
texto = "normal estándar"
plt.plot(x, y, label=texto)

plt.xlabel("x")
plt.ylabel("densidad de probabilidad")
plt.legend(loc='upper right')
plt.grid()
plt.savefig('../graficos/t_pdf.png')

plt.show()
