# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
parametros = [(0.5, 0.5), (5, 1), (1, 3), (2, 5)]
for alpha1, alpha2 in parametros:
    x = np.arange(start=0, stop=1, step=0.01)
    distribucion = scipy.stats.beta(alpha1, alpha2)
    y = distribucion.pdf(x)
    texto = r'$\alpha_1=$' + str(alpha1) + r',$\alpha_2=$' + str(alpha2)
    plt.plot(x, y, label=texto)

plt.xlabel("x")
plt.ylabel("densidad de probabilidad")
plt.legend(loc='upper center')
plt.grid()
plt.savefig('../graficos/beta_pdf.png')

plt.show()
