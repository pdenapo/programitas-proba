# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
for n in range(1, 6):
    x = np.arange(start=0.1, stop=15, step=0.01)
    distribucion = scipy.stats.chi2(n)
    y = distribucion.cdf(x)
    texto = r'n=' + str(n)
    plt.plot(x, y, label=texto)

plt.xlabel("x")
plt.ylabel("probabilidad")
plt.legend(loc='lower right')
plt.grid()
plt.savefig('../graficos/chi2_cdf.png')

plt.show()
