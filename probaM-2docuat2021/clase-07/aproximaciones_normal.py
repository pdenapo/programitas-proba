import argparse
from numpy import sqrt
from scipy.stats import binom, poisson, norm


def aproximar_normal(k, n, p):
    print("k=", k)
    print("n=", n)
    print("p=", p)
    exacto = binom(n, p).pmf(k)
    mu = n * p
    q = 1 - p
    sigma = sqrt(n * p * q)
    aprox_poisson = poisson(mu).pmf(k)
    aprox_normal = norm(mu, sigma).pdf(k)
    err_rel_poisson = (aprox_poisson - exacto) / exacto
    err_rel_normal = (aprox_normal - exacto) / exacto
    print("Valor exacto = \t\t", exacto)
    print("Aprox. de Poisson=\t", aprox_poisson)
    print("Error rel. Poisson=\t", err_rel_poisson)
    print("Aprox. normal=\t\t", aprox_normal)
    print("Error relativo Normal=\t", err_rel_normal)


if __name__ == "__main__":
    global mostrar_trayectoria

    parser = argparse.ArgumentParser(
        description='Aproximaciones de la distribución binomial')
    parser.add_argument("k", action='store', help='número de éxitos', type=int)
    parser.add_argument("n",
                        action='store',
                        help='número de ensayos',
                        type=int)
    parser.add_argument("p", action='store', help='probabilidad', type=float)
    args = parser.parse_args()
    aproximar_normal(args.k, args.n, args.p)
