# usar python3 
import numpy as np
import scipy.stats 
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
n=20
p=0.4
binomial=scipy.stats.binom(n,p)
x=np.arange(start=0,stop=n)
x_denso=np.arange(start=0,stop=n,step=0.1)
y=binomial.pmf(x)
esperanza=n*p
varianza=n*p*(1-p)
normal=scipy.stats.norm(loc=esperanza,scale=np.sqrt(varianza))
z=normal.pdf(x_denso)
maximo_y=0.20
plt.axis([0, 20, 0, maximo_y])
plt.xlabel("k")
plt.ylabel("probabilidad")
#plt.title("La distribución binomial con n="+str(n)+" y p="+str(p))
plt.plot(x,y,'ro')
plt.text(13,maximo_y*0.9,'Distribución binomial',color='red')
plt.text(13,maximo_y*0.85,'con n='+str(n)+",p="+str(p),color='red')
plt.plot(x_denso,z,color='blue')
plt.text(13,maximo_y*0.7,'Aproximación por la normal',color='blue')
plt.show() 