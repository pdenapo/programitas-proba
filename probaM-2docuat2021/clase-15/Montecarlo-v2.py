#!/usr/bin/env python3
# coding: utf-8

# # El método de Montecarlo

from numba import jit
import numpy as np
import time

@jit(nopython=True)
def f(x:np.double)-> np.double:
 return 4*np.sqrt(1-x**2)

@jit(nopython=True)
def Montecarlo(f,a:np.float,b:np.float,n:int):
    s=0
    for i in range(0,n):
        u= np.random.sample()*(b-a)+a
        s += f(u)
    return s/n


M=4 
delta=0.001
p=0.99
q=1-p
n=np.int64(np.ceil(4*M**2/(delta**2*q)))
start = time.time()
print(Montecarlo(f,0,1,n))
end = time.time()
print("Tardamos ", end-start, "segundos.")




