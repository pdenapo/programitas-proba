#!/usr/bin/env python3
# coding: utf-8

# # El método de Montecarlo

import numpy as np
import time

def f(x:np.double)-> np.double:
 return 4*np.sqrt(1-x**2)

def Montecarlo(f,a:float,b:float,n:int):
    s:float=0
    for i in range(0,n):
        u= np.random.sample()*(b-a)+a
        s += f(u)
    return s/n


M=4 
delta=0.1
p=0.99
q=1-p
n=np.int64(np.ceil(4*M**2/(delta**2*q)))
start = time.time()
print(Montecarlo(f,0,1,n))
end = time.time()
print("Tardamos ", end-start, "segundos.")




