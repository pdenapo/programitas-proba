
# Muestra las curvas de nivel de una forma cuadrática.

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt


delta = 0.025
x = np.arange(-10.0, 10.0, delta)
y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = X**2+Y**2-X*Y
niveles=np.arange(1,100,10)
plt.contourf(X, Y, Z, cmap='viridis',levels=niveles)
plt.xlabel("x1")
plt.ylabel("x2")
plt.savefig("../graficos/forma_cuadratica.png")
plt.show()
        
