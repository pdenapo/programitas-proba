
# grafico de la normal bivariada 
def f(x,y):
 return 1/(2*pi)*math.exp(-(x^2+y^2)/2)

P = plot3d(f,(-3,3),(-3,3), adaptive=True, color=rainbow(120, 'rgbtuple'), max_bend=.1, max_depth=15)
P.show()