from numpy.random import default_rng
rng = default_rng(100)  #semilla
from scipy.stats import norm
print(norm.rvs(size=5, random_state=rng))
