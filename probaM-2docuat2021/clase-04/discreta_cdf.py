# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
xk = (1, 2, 3)
pk = (0.5, 0.25, 0.25)
distribucion = scipy.stats.rv_discrete(values=(xk, pk))
x = np.arange(start=-1, stop=5, step=0.01)
y = distribucion.cdf(x)
y_maximo = max(y) * 1.1
plt.axis([-1, 5, 0, y_maximo])
plt.xlabel("x")
plt.ylabel("probabilidad")
texto = 'La función de distribución de una variable aleatoria discreta'
plt.text(10, 0.08, texto, color='r')
grafico = plt.plot(x, y, 'ro')
plt.show()
