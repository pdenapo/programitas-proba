# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
p = 0.1
hasta = 30
distribucion = scipy.stats.geom(p)
x = np.arange(start=0, stop=hasta)
y = distribucion.pmf(x)
y_maximo = max(y) * 1.1
plt.axis([0, hasta, 0, y_maximo])
plt.xlabel("k")
plt.ylabel("probabilidad")
texto = 'La distribución geometrica con p=' + str(p)
plt.text(10, 0.08, texto, color='r')
grafico = plt.plot(x, y, 'ro')
plt.show()
