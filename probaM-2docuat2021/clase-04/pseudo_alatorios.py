# simula el generador de números pseudo-aleatorios de Lehmer 
# usado por el ZX-spectrum
# https://en.wikipedia.org/wiki/Lehmer_random_number_generator

import numpy as np

p= 2**16+1
g= 75
x= 1 #semilla del generador
def generador():
   global x
   x=(x*g)%p
   return x

limite=6
cuantas_veces=1000
veces= np.zeros(limite,dtype=int)

for k in range(0,cuantas_veces):
    dado=generador()%limite
    veces[dado]+=1

print("valor \tveces \tfrecuencia")
for j in range(0,limite):
    print(j,"\t",veces[j],"\t",veces[j]/cuantas_veces)


