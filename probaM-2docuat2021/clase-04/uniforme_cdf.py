# usar python3
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
distribucion = scipy.stats.uniform
x = np.arange(start=-1, stop=2, step=0.01)
y = distribucion.cdf(x)
y_maximo = max(y) * 1.1
plt.axis([-1, 2, 0, y_maximo])
plt.xlabel("k")
plt.ylabel("probabilidad")
texto = 'La distribución uniforme en [0,1]'
plt.text(10, 0.08, texto, color='r')
grafico = plt.plot(x, y, 'ro')
plt.show()
